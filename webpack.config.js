const path = require('path');
const pkg = require('./package.json');

module.exports = {
  entry: './tracker/index.js',
  output: {
    filename: `open-analytics-${pkg.version}.min.js`,
    path: path.resolve(__dirname, 'dist'),
  },
  devtool: process.env.NODE_ENV === 'production' ? false : 'eval-source-map',
};
