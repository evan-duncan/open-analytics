import version from '../../tracker/version.js';
import assert from 'assert'

describe('Version', () => {
    it('should be a semantic version string', () => {
        assert.match(version, /\d+\.\d+\.\d+/i)
    });
});
