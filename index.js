const express = require('express');
const logger = require('express-pino-logger');
const helmet = require('helmet');

const app = express();

app.use('/public', express.static(__dirname + '/public'));
app.use('/tracker', express.static(__dirname + '/dist'));
app.use(logger());
app.use(helmet());

app.set('view engine', 'pug');

app.get('/', (req, res) =>
  res.render('index', { title: 'Open Analytics' }));

app.listen(3000, console.log(`App listening on port 3000`));
